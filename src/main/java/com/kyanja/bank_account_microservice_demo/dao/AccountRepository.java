package com.kyanja.bank_account_microservice_demo.dao;


import com.kyanja.bank_account_microservice_demo.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findBySortCodeAndAccountNumber(String sortCode, String accountNumber);
    Optional<Account> findByAccountNumber(String accountNumber);
    void deleteByAccountNumber(Long accountNumber);

}
