package com.kyanja.bank_account_microservice_demo.controller;


import com.kyanja.bank_account_microservice_demo.dto.AccountDto;
import com.kyanja.bank_account_microservice_demo.exception.ResourceNotFoundException;
import com.kyanja.bank_account_microservice_demo.model.Account;
import com.kyanja.bank_account_microservice_demo.service.AccountService;
import com.kyanja.bank_account_microservice_demo.utils.AccountInput;
import com.kyanja.bank_account_microservice_demo.utils.Constants;
import com.kyanja.bank_account_microservice_demo.utils.CreateAccountInput;
import com.kyanja.bank_account_microservice_demo.utils.InputValidator;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/accounts")
public class AccountRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountRestController.class);

    private final ModelMapper modelMapper;

    private final  AccountService accountService;

    @Autowired
    public AccountRestController(AccountService accountService,ModelMapper modelMapper) {
        this.accountService = accountService;
        this. modelMapper =  modelMapper;
    }

    @RequestMapping(value = "/accounts-list", method = RequestMethod.GET)
    public List<AccountDto> getBankAccounts() {
        return accountService.getAllAccounts().stream().map(post -> modelMapper.map(post, AccountDto.class))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/save-account", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountDto> createAccount(@RequestBody AccountDto accountDto ) throws ResourceNotFoundException {

        // convert DTO to entity
        Account accountRequest = modelMapper.map(accountDto, Account.class);

        Account account = accountService.createAccount(accountRequest);

        // convert entity to DTO
        AccountDto accountResponse = modelMapper.map(account, AccountDto.class);

        return new ResponseEntity<AccountDto>(accountResponse, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AccountDto> updateAccount(@PathVariable Long id, @RequestBody AccountDto accountDto) throws ResourceNotFoundException {

        // convert DTO to Entity
        Account accountRequest = modelMapper.map(accountDto, Account.class);

        Account account = accountService.modifyAccount(id, accountRequest);

        // entity to DTO
        AccountDto accountResponse = modelMapper.map(account, AccountDto.class);

        return ResponseEntity.ok().body(accountResponse);
    }

    @PostMapping(value = "/accounts",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> checkAccountBalance(
            // TODO In the future support searching by card number in addition to sort code and account number
            @Valid @RequestBody AccountInput accountInput) {
        LOGGER.debug("Triggered AccountRestController.accountInput");

        // Validate input
        if (InputValidator.isSearchCriteriaValid(accountInput)) {
            // Attempt to retrieve the account information
            Optional<Account> account = accountService.getAccount(
                    accountInput.getSortCode(), accountInput.getAccountNumber());

            // Return the account details, or warn that no account was found for given input
            if (account == null) {
                return new ResponseEntity<>(Constants.NO_ACCOUNT_FOUND, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(account, HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(Constants.INVALID_SEARCH_CRITERIA, HttpStatus.BAD_REQUEST);
        }
    }


    @PutMapping(value = "/accounts",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createAccount(
            @Valid @RequestBody CreateAccountInput createAccountInput) {
        LOGGER.debug("Triggered AccountRestController.createAccountInput");

        // Validate input
        if (InputValidator.isCreateAccountCriteriaValid(createAccountInput)) {
            // Attempt to retrieve the account information
            Account account = accountService.createAccount(
                    createAccountInput.getBankName(), createAccountInput.getOwnerName());

            // Return the account details, or warn that no account was found for given input
            if (account == null) {
                return new ResponseEntity<>(Constants.CREATE_ACCOUNT_FAILED, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(account, HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(Constants.INVALID_SEARCH_CRITERIA, HttpStatus.BAD_REQUEST);
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        return errors;
    }

    @DeleteMapping("/delete/{accountNumber}")
    public ResponseEntity<HttpStatus> deleteAccount(@PathVariable("accountNumber") Long accountNumber) {
        try {
            accountService.deleteAccountByAccountNumber(accountNumber);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
