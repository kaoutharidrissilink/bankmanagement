package com.kyanja.bank_account_microservice_demo.type;

public enum OperationType {

    DEPOSIT,
    WITHDRAW
}
