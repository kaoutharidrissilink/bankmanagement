package com.kyanja.bank_account_microservice_demo.utils;

public class ApiResponse<T> {


    private boolean status;
    private String message;
    private Object result;

    public ApiResponse(boolean status, String message, Object result) {
        this.status = status;
        this.message = message;
        this.result = result;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

}
