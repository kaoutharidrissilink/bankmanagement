package com.kyanja.bank_account_microservice_demo.service;

import com.kyanja.bank_account_microservice_demo.exception.ResourceNotFoundException;
import com.kyanja.bank_account_microservice_demo.model.Account;

import java.util.List;
import java.util.Optional;

public interface AccountService {

    List<Account> getAllAccounts();

    Optional<Account> getAccountById(Long accountNumber) throws ResourceNotFoundException;

    Account createAccount( Account account) throws ResourceNotFoundException;

    Account modifyAccount(Long accountId, Account account) throws ResourceNotFoundException;

    void deleteAccountById(Long accountId) throws ResourceNotFoundException;

    Optional<Account> getAccount(String accountNumber);

    Optional<Account> getAccount(String sortCode, String accountNumber);

    Account createAccount(String bankName, String ownerName);

    void deleteAccountByAccountNumber(Long accountNumber) throws ResourceNotFoundException;




}
