package com.kyanja.bank_account_microservice_demo.service;

import com.kyanja.bank_account_microservice_demo.model.Account;
import com.kyanja.bank_account_microservice_demo.type.OperationType;
import com.kyanja.bank_account_microservice_demo.utils.TransactionInput;

import java.util.Optional;

public interface TransactionService {

    public boolean makeTransfer(TransactionInput transactionInput);

    public void updateAccountBalance(Optional<Account> account, double amount, OperationType action);

    public boolean isAmountAvailable(double amount, double accountBalance);


}
