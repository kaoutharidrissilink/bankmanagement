package com.kyanja.bank_account_microservice_demo.service.impl;


import com.kyanja.bank_account_microservice_demo.dao.AccountRepository;
import com.kyanja.bank_account_microservice_demo.dao.TransactionRepository;
import com.kyanja.bank_account_microservice_demo.exception.ResourceNotFoundException;
import com.kyanja.bank_account_microservice_demo.model.Account;
import com.kyanja.bank_account_microservice_demo.service.AccountService;
import com.kyanja.bank_account_microservice_demo.utils.CodeGenerator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;

    public AccountServiceImpl(AccountRepository accountRepository,
                          TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }


    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }


    @Override
    public Optional<Account> getAccountById(Long accountNumber) throws ResourceNotFoundException {
        if (!accountRepository.existsById(accountNumber)) {
            throw new ResourceNotFoundException("Account Number: " + accountNumber + " not found");
        }
        return accountRepository.findById(accountNumber);
    }

    @Override
    public Account createAccount(Account account) throws ResourceNotFoundException {

        boolean accountIsExists = accountRepository.existsById(account.getAccountNumber());

        if (!accountIsExists) {
            Account accountToCreate = new Account();
            accountToCreate.setAccountNumber(account.getAccountNumber());
            accountToCreate.setClient(account.getClient());
            accountToCreate.setCurrentBalance(account.getCurrentBalance());
            return accountRepository.save(accountToCreate);
        } else {
            throw new ResourceNotFoundException("Account Number with id " + account.getAccountNumber() + " does already exist");
        }
    }

    @Override
    public Account modifyAccount(Long accountId,Account account) throws ResourceNotFoundException {

        boolean accountIsExists = accountRepository.existsById(account.getAccountNumber());

        if (accountIsExists) {
            return accountRepository.save(account);
        } else {
            throw new ResourceNotFoundException("Account Number with id " + account.getAccountNumber() + " does not exist");
        }
    }

    @Override
    public void deleteAccountById(Long accountId) throws ResourceNotFoundException {
        if (!accountRepository.existsById(accountId)) {
            throw new ResourceNotFoundException("Account with id " + accountId + " not found");
        }
        accountRepository.deleteById(accountId);

    }

    public Optional<Account> getAccount(String sortCode, String accountNumber) {
        Optional<Account> account = accountRepository
                .findBySortCodeAndAccountNumber(sortCode, accountNumber);

        account.ifPresent(value ->
                value.setTransactions(transactionRepository
                        .findBySourceAccountIdOrderByInitiationDate(value.getId())));

        return Optional.ofNullable(account.orElse(null));
    }

    public Optional<Account> getAccount(String accountNumber) {
        Optional<Account> account = accountRepository
                .findByAccountNumber(accountNumber);

        return Optional.ofNullable(account.orElse(null));
    }

    public Account createAccount(String bankName, String ownerName) {
        CodeGenerator codeGenerator = new CodeGenerator();
        Account newAccount = new Account(bankName, ownerName, codeGenerator.generateSortCode(), codeGenerator.generateAccountNumber(), 0.00);
        return accountRepository.save(newAccount);
    }

    @Override
    public void deleteAccountByAccountNumber(Long accountNumber) throws ResourceNotFoundException {


        accountRepository.deleteByAccountNumber(accountNumber);


    }







}
