package com.kyanja.bank_account_microservice_demo.service.impl;

import com.kyanja.bank_account_microservice_demo.dao.AccountRepository;
import com.kyanja.bank_account_microservice_demo.dao.TransactionRepository;
import com.kyanja.bank_account_microservice_demo.model.Account;
import com.kyanja.bank_account_microservice_demo.model.Transaction;
import com.kyanja.bank_account_microservice_demo.service.TransactionService;
import com.kyanja.bank_account_microservice_demo.type.OperationType;
import com.kyanja.bank_account_microservice_demo.utils.TransactionInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Transactional
public class TransactionServiceImpl   implements TransactionService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;


    @Override
    public boolean makeTransfer(TransactionInput transactionInput) {
        String sourceSortCode = transactionInput.getSourceAccount().getSortCode();
        String sourceAccountNumber = transactionInput.getSourceAccount().getAccountNumber();
        Optional<Account> sourceAccount = accountRepository
                .findBySortCodeAndAccountNumber(sourceSortCode, sourceAccountNumber);

        String targetSortCode = transactionInput.getTargetAccount().getSortCode();
        String targetAccountNumber = transactionInput.getTargetAccount().getAccountNumber();
        Optional<Account> targetAccount = accountRepository
                .findBySortCodeAndAccountNumber(targetSortCode, targetAccountNumber);

        if (sourceAccount.isPresent() && targetAccount.isPresent()) {
            if (isAmountAvailable(transactionInput.getAmount(), sourceAccount.get().getCurrentBalance())) {
                Transaction transaction = new Transaction();

                transaction.setAmount(transactionInput.getAmount());
                transaction.setSourceAccountId(sourceAccount.get().getId());
                transaction.setTargetAccountId(targetAccount.get().getId());
                transaction.setTargetOwnerName(targetAccount.get().getClient());
                transaction.setInitiationDate(LocalDateTime.now());
                transaction.setCompletionDate(LocalDateTime.now());
                transaction.setReference(transactionInput.getReference());
                transaction.setLatitude(transactionInput.getLatitude());
                transaction.setLongitude(transactionInput.getLongitude());

                updateAccountBalance(Optional.of(sourceAccount.get()), transactionInput.getAmount(), OperationType.WITHDRAW);
                transactionRepository.save(transaction);

                return true;
            }
        }
        return false;
    }

    @Override
    public void updateAccountBalance(Optional<Account> account, double amount, OperationType action) {

        if (action ==  OperationType.WITHDRAW) {
            account.get().setCurrentBalance((account.get().getCurrentBalance() - amount));
        } else if (action ==  OperationType.DEPOSIT) {
            account.get().setCurrentBalance((account.get().getCurrentBalance() + amount));
        }
        accountRepository.save(account.get());
    }

    @Override
    public boolean isAmountAvailable(double amount, double accountBalance) {
        return (accountBalance - amount) > 0;
    }
}
