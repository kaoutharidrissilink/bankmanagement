package com.kyanja.bank_account_microservice_demo.dto;

import java.io.Serializable;

public class AccountDto implements Serializable {

    private Long accountNumber;
    private String client;
    private double currentBalance;

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }


    public double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public AccountDto() {
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "accountNumber=" + accountNumber +
                ", client='" + client + '\'' +
                ", currentBalance=" + currentBalance +
                '}';
    }
}
