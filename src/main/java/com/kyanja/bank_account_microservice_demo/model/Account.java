package com.kyanja.bank_account_microservice_demo.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Account")
public class Account {


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "account_id")
    private Long id;
    @Column(unique=true)
    private Long accountNumber;
    @Column
    private String client;
    @Column
    private Double currentBalance;
    @Column
    private String sortCode;
    @Column
    private String bankName;
    @Column
    private String ownerName;



    @OneToMany(mappedBy="account")
    private List<Transaction> transactions;

    public Account(String bankName, String ownerName, String generateSortCode, String generateAccountNumber, double v) {
    }

    public Account(long l, String s, String s1, double v, String some_bank, String john) {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", accountNumber=" + accountNumber +
                ", client='" + client + '\'' +
                ", currentBalance=" + currentBalance +
                ", sortCode='" + sortCode + '\'' +
                ", bankName='" + bankName + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", transactions=" + transactions +
                '}';
    }

    public Account() {
    }


}

